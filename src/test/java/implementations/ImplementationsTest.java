package implementations;

import conection.Conection;
import enums.Edit;
import implementation.EmployeeDao;
import interfaces.EmployeeInter;
import model.Employee;
import org.junit.Assert;
import org.junit.Test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ImplementationsTest {
    public static Integer ID = 20;
    @Test
    public void aCreateTest(){
        EmployeeInter employeeDao= new EmployeeDao();
        Employee employee = new Employee(ID, "Ruben Arturo", "Gonzalez", "rhorgc@gmail.com",new Double("30000"));
        employeeDao.create(employee);

    }

    @Test
    public void bReadAllTest(){
        EmployeeInter employeeInter = new EmployeeDao();
        List<Employee> employees = employeeInter.readAll();
        for (Employee emp : employees){
            System.out.println(emp);
            Assert.assertNotNull(employees);
        }

    }

    @Test
    public void bReadCriteriaTest(){
        EmployeeInter employeeInter= new EmployeeDao();
        String ru = "Ru";
        List<Employee> employees = employeeInter.readCriteria(ru);
        
        for (Employee employee : employees){
            System.out.println(employee);
           
        }

    }

    @Test
    public void dReadByIdTest(){
        EmployeeInter employeeDao= new EmployeeDao();
        Employee employee = employeeDao.read(ID);
        Assert.assertNotNull(employee);
        System.out.println(employee);
    }

    @Test
    public void eUpdateTest(){
        EmployeeInter employeeInter= new EmployeeDao();
        Employee employee = employeeInter.read(ID);
        employee.setLast_name("Normand");
        employeeInter.update(employee);
        String nameToTry = "Normand";
        Assert.assertEquals(employee.getLast_name(), nameToTry);
        System.out.println("employee = " + employee);
    }

    @Test
    public void eDeleteTest(){
        EmployeeInter employeeInter= new EmployeeDao();
        employeeInter.delete(ID);

    }
}
