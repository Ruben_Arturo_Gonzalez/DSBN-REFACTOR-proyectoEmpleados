package model;

import enums.Edit;

public class Employee extends Model{

    public static final String FIELDS = "id, first_name, last_name, email, salary";
    public static final String TABLE = "employees";
    public static final String Q_ALL = String.format("SELECT %s FROM %s", FIELDS, TABLE);
    public static final String Q_BY_ID = String.format("%s WHERE id = ", Q_ALL);
    public static final String INSERT = String.format("INSERT INTO %s (%s) VALUES %s", TABLE, FIELDS, fieldsToInsert(5));
    public static final String UPDATE = String.format("UPDATE %s SET id = ?, first_name = ?, last_name = ?, email = ?, salary = ? WHERE id =", TABLE);
    public static final String DELETE = String.format("DELETE FROM %s WHERE id = ?", TABLE);
    public static final String LIKE = String.format("%s where first_name like ?",Q_ALL);

    private Integer id;
    private String first_name;
    private String last_name;
    private String email;
    private Double salary;


    public Employee() {
    }

    public Employee(Integer id, String first_name, String last_name, String email, Double salary) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.salary = salary;

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }



    @Override
    public String toString() {
        return String.format("%s - %s - %s - %s - %s", this.getId(), this.getFirst_name(), this.getLast_name(), this.getEmail(), this.getSalary());
    }
}
