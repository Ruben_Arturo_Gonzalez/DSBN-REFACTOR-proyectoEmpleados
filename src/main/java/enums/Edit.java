package enums;

public enum Edit {
    YES(Boolean.TRUE),
    NO(Boolean.FALSE);

    private Boolean edit;

    Edit(Boolean edit){
        this.edit = edit;
    }

    public Boolean getEdit(){
        return edit;
    }
}
