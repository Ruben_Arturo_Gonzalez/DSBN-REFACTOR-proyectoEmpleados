package interfaces;

import model.Employee;

import java.util.List;

public interface EmployeeInter {

    public void create(Employee obj);

    public List<Employee> readAll();

    public List<Employee> readCriteria(String criteria);

    public Employee read(Integer id);

    public void update(Employee obj);

    public void delete(Integer id);
}
