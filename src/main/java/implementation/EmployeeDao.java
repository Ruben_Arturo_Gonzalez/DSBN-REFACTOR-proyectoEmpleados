package implementation;

import conection.Conection;
import enums.Edit;
import interfaces.EmployeeInter;
import model.Employee;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EmployeeDao implements EmployeeInter{
    @Override
    public void create(Employee employee) {
        try {
            Conection conexion = Conection.getInstance();
            PreparedStatement ps = conexion.getConn().prepareStatement(Employee.INSERT);
            Integer i =1;
            ps.setLong(i++, employee.getId());
            ps.setString(i++, employee.getFirst_name());
            ps.setString(i++, employee.getLast_name());
            ps.setString(i++, employee.getEmail());
            ps.setDouble(i++, employee.getSalary());
            ps.executeUpdate();
        } catch (ClassNotFoundException | SQLException e) {

        }
    }

    @Override
    public List<Employee> readAll() {
        List<Employee> employees = new ArrayList<>();
        try {
            Conection conexion = Conection.getInstance();
            Statement st = conexion.getConn().createStatement();
            ResultSet rs = st.executeQuery(String.format("%s",Employee.Q_ALL));
            while(rs.next()){
                employees.add(makeEmployees(rs));
            }
        }catch (ClassNotFoundException | SQLException ex){

        }
        return employees;
    }

    @Override
    public List<Employee> readCriteria(String criteria) {
         List<Employee> employees = new ArrayList<>();
         String searchCriteria = "ub";
        try {
             Conection conexion = Conection.getInstance();
            PreparedStatement ps = conexion.getConn().prepareStatement(Employee.LIKE);
            ps.setString(1, "%" + criteria + "%");
            ResultSet rs = ps.executeQuery();
             while(rs.next()){
                employees.add(makeEmployees(rs));
            }
        } catch (ClassNotFoundException | SQLException ex) {
        }
        return employees;
        
    }

    @Override
    public Employee read(Integer id) {
        Employee employee = null;
        try {
            Connection conexion = Conection.getInstance().getConn();
            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery(String.format("%s %s",Employee.Q_BY_ID, id));
            if(rs.next()){
                employee = makeEmployees(rs);
            }
        }catch (ClassNotFoundException | SQLException ex){

        }
        return employee;
    }

    @Override
    public void update(Employee employee) {
        try {
            Conection conexion = Conection.getInstance();
            PreparedStatement ps = conexion.getConn()
                    .prepareStatement(String.format("%s %s",Employee.UPDATE, employee.getId()));
            Integer i =1;
            ps.setLong(i++, employee.getId());
            ps.setString(i++, employee.getFirst_name());
            ps.setString(i++, employee.getLast_name());
            ps.setString(i++, employee.getEmail());
            ps.setDouble(i++, employee.getSalary());
            ps.executeUpdate();
        } catch (ClassNotFoundException | SQLException e) {

        }

    }

    @Override
    public void delete(Integer id) {

        try {
            Conection conexion = Conection.getInstance();
            PreparedStatement ps = conexion.getConn()
                    .prepareStatement(Employee.DELETE);
            Integer i =1;
            ps.setLong(i++, id);
            ps.executeUpdate();
        } catch (ClassNotFoundException | SQLException e) {

        }

    }

    private Employee makeEmployees(ResultSet rs) throws SQLException {
        Employee employee = new Employee();
        Integer i = 1;
        employee.setId(rs.getInt(i++));
        employee.setFirst_name(rs.getString(i++));
        employee.setLast_name(rs.getString(i++));
        employee.setEmail(rs.getString(i++));
        employee.setSalary(rs.getDouble(i++));

        return employee;
    }
}
